class UnsupportedInput(Exception):
	pass

class UnsupportedOutput(Exception):
	pass

class NoInputConfigured(Exception):
	pass

class NoOutputConfigured(Exception):
	pass

class AmbientOrb(object):
	"""
	Periodically traverse configured server list for build failures
	and update feedback mechanisms.

	USAGE

	ao = AmbientOrb()
	ao.add_input("http://localhost:8080", "jenkins")
	ao.run()

	INPUTS

	Supported servers

	Currently only Jenkins is supported.
	When adding a jenkins server, only add the URL to the Jenkins instance.
	The logic inside this object will then construct the URL to the failure
	RSS feed.

	OUTPUTS

	stderr
	"""

	def __init__(self):
		# Capabilities
		from inputs import JenkinsRSS
		self.inputs_cls = {'jenkins': JenkinsRSS}
		self._inputs_supported = self.inputs_cls.keys()
		from outputs import Console
		from outputs import IFTTT_ColorLightBulb
		self.outputs_cls = {'console': Console, 'ifttt': IFTTT_ColorLightBulb}
		self._outputs_supported = self.outputs_cls.keys()
		# Defaults
		self._default_outputs_configured = ["console"]
		# Settings
		self._inputs_configured = []
		self._outputs_configured = []
		# Set up outputs from defaults
		for o in self._default_outputs_configured:
			self.add_output(o)

	def get_inputs_supported(self):
		"""
		Get list of kinds of CI server supported.
		"""
		# FIXME Have test to prove we need copy.copy
		return self._inputs_supported

	def get_outputs_supported(self):
		"""
		Get list of supported output methods.
		"""
		# FIXME Have test to prove we need copy.copy
		return self._outputs_supported

	def get_inputs_configured(self):
		"""
		Get list of all configured servers to observe.
		"""
		# FIXME Have test to prove we need copy.copy
		return self._inputs_configured

	def get_outputs_configured(self):
		"""
		Get list of feedback methods configured.
		"""
		# FIXME Have test to prove we need copy.copy
		return self._outputs_configured

	def add_input(self, kind, url):
		"""
		Add server to list of servers to be observed.
		"""
		if kind not in self._inputs_supported:
			raise UnsupportedInput
		# FIXME Have test to prove we DO NOT need copy.copy
		entry = {'url': url, 'kind': kind}
		self._inputs_configured.append(entry)

	def add_output(self, kind, args={}):
		"""
		Add output to list of configured outputs.
		"""
		if kind not in self._outputs_supported:
			raise UnsupportedOutput
		# FIXME Have test to prove we DO NOT need copy.copy
		entry = {'kind': kind, 'args': args}
		self._outputs_configured.append(entry)

	def _dispatch_inputs(self):
		report = []
		for entry in self._inputs_configured:
			kind = entry['kind']
			url = entry['url']
			cls = self.inputs_cls[kind]
			i = cls()
			i.set_base_url(url)
			events_reported_by_input = i.has_failed()
			if not events_reported_by_input:
				continue
			for event in events_reported_by_input:
				report_entry = {'kind': kind, 'url': url}
				report_entry.update(event)
				report.append(report_entry)
		return report

	def _dispatch_outputs(self, report):
		for entry in self._outputs_configured:
			kind = entry['kind']
			args = entry['args']
			cls = self.outputs_cls[kind]
			o = cls(**args)
			o.update(report)

	def run(self):
		"""
		Start operation of checking servers and updating feedback.
		"""
		if len(self._inputs_configured) == 0:
			raise NoInputConfigured
		if len(self._outputs_configured) == 0:
			raise NoOutputConfigured
		report = self._dispatch_inputs()
		self._dispatch_outputs(report)
