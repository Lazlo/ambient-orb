class Console(object):

	def __init__(self, **kwargs):
		return

	def stderr_write(self, msg):
		import sys
		sys.stderr.write("%s\n" % msg)
		sys.stderr.flush()

	def report_failure(self, source):
		msg = "Failure reported by %s!" % source
		self.stderr_write(msg)

	def update(self, report):
		if len(report) == 0:
			return
		for report_entry in report:
			msg = report_entry['msg']
			self.report_failure(msg)
