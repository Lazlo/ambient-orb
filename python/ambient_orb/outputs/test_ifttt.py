import unittest
from ifttt import IFTTT_ColorLightBulb

class IFTTT_ColorLightBulb_BaseTestCase(unittest.TestCase):

	def setUp(self):
		self.expected_key = "009b87056069b021338d6b7661bf5b07"
		self.expected_event_name = "build_job_failed"
		self.expected_url = "https://maker.ifttt.com/trigger/%s/with/key/%s" % (self.expected_event_name, self.expected_key)
		self.expected_kwargs = {}
		self.expected_kwargs.update({'key': self.expected_key})
		self.expected_kwargs.update({'event_name': self.expected_event_name})
		self.o = IFTTT_ColorLightBulb(**self.expected_kwargs)

	def clear_key(self):
		self.o._key = None

	def clear_event_name(self):
		self.o._event_name = None

class init_wihout_args(unittest.TestCase):

	def setUp(self):
		self.o = IFTTT_ColorLightBulb()

	def test_init_sets_key_to_none(self):
		self.assertEqual(None, self.o._key)

	def test_init_sets_event_name_to_none(self):
		self.assertEqual(None, self.o._event_name)

class init_with_kwargs(IFTTT_ColorLightBulb_BaseTestCase):

	def test_init_sets_key_from_kwarg(self):
		self.assertEqual(self.expected_key, self.o._key)

	def test_init_sets_event_name_from_kwargs(self):
		self.assertEqual(self.expected_event_name, self.o._event_name)

class init_called_with_invalid_kwarg_raises_exception(IFTTT_ColorLightBulb_BaseTestCase):

	def test_init_raises_exception_on_invalid_kwarg(self):
		from ifttt import InvalidKeywordArgument
		self.assertRaises(InvalidKeywordArgument, IFTTT_ColorLightBulb, **{'invalid_key': "random-value"})

class set_key(IFTTT_ColorLightBulb_BaseTestCase):

	def test_set_key(self):
		self.clear_key()
		self.o.set_key(self.expected_key)
		self.assertEqual(self.expected_key, self.o._key)

class set_event_name(IFTTT_ColorLightBulb_BaseTestCase):

	def test_set_event_name(self):
		self.clear_event_name()
		self.o.set_event_name(self.expected_event_name)
		self.assertEqual(self.expected_event_name, self.o._event_name)

class get_url(IFTTT_ColorLightBulb_BaseTestCase):

	def test_get_url(self):
		self.assertEqual(self.expected_url, self.o.get_url())

fake_urlopen_called = None
fake_urlopen_arg = None

def fake_urlopen(arg):
	global fake_urlopen_called
	global fake_urlopen_arg
	fake_urlopen_called = True
	fake_urlopen_arg = arg

import urllib2

class update(IFTTT_ColorLightBulb_BaseTestCase):

	def install_fake_urlopen(self):
		self.real_urlopen = urllib2.urlopen
		urllib2.urlopen = fake_urlopen

	def restore_real_urlopen(self):
		urllib2.urlopen = self.real_urlopen

	def test_update_raises_exception_when_no_key_is_set(self):
		self.clear_key()
		from ifttt import NoKeySet
		report = []
		self.assertRaises(NoKeySet, self.o.update, report)

	def test_update_raises_exception_when_no_event_name_is_set(self):
		self.clear_event_name()
		from ifttt import NoEventNameSet
		report = []
		self.assertRaises(NoEventNameSet, self.o.update, report)

	def test_update_calls_urllib2_urlopen(self):
		global fake_urlopen_called
		fake_urlopen_called = False
		self.install_fake_urlopen()
		report = [{}, {}, {}]
		self.o.update(report)
		self.restore_real_urlopen()
		self.assertEqual(True, fake_urlopen_called)

	def test_update_calls_urllib2_urlopen_with_url_for_event_name_build_failed_when_report_is_not_empty(self):
		global fake_urlopen_arg
		fake_urlopen_arg = None
		self.install_fake_urlopen()
		expected = "https://maker.ifttt.com/trigger/build_failed/with/key/%s" % self.o._key
		report = [{}, {}, {}]
		self.o.update(report)
		self.restore_real_urlopen()
		self.assertEqual(expected, fake_urlopen_arg)

	def test_update_calls_urllib2_urlopen_with_url_for_event_name_build_passed_when_report_is_empty(self):
		global fake_urlopen_arg
		fake_urlopen_arg = None
		self.install_fake_urlopen()
		expected = "https://maker.ifttt.com/trigger/build_passed/with/key/%s" % self.o._key
		report = []
		self.o.update(report)
		self.restore_real_urlopen()
		self.assertEqual(expected, fake_urlopen_arg)
