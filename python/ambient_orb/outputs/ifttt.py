class InvalidKeywordArgument(Exception):
	pass

class NoKeySet(Exception):
	pass

class NoEventNameSet(Exception):
	pass

class IFTTT_Base():

	def __init__(self, **kwargs):
		# Parse keyword arguments
		key = None
		event_name = None
		for arg_key in kwargs.keys():
			arg_val = kwargs[arg_key]
			if arg_key is 'key':
				key = arg_val
			elif arg_key is 'event_name':
				event_name = arg_val
			else:
				raise InvalidKeywordArgument
		self._key = key
		self._event_name = event_name

	def set_key(self, key):
		self._key = key

	def set_event_name(self, event_name):
		self._event_name = event_name

	def get_url(self):
		url = "https://maker.ifttt.com/trigger/%s/with/key/%s" % (self._event_name, self._key)
		return url

	def update(self, report):
		if self._key == None:
			raise NoKeySet
		if self._event_name == None:
			raise NoEventNameSet
		# FIXME Here we are breaking the use cases of our own class.
		# This is a strong sign we need to redesign the class.
		if len(report) == 0:
			self.set_event_name("build_passed")
		else:
			self.set_event_name("build_failed")
		url = self.get_url()
		import urllib2
		urllib2.urlopen(url)
		# TODO Save response
		# TODO Parse response using JSON

class IFTTT_ColorLightBulb(IFTTT_Base):
	pass
