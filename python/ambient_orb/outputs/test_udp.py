import unittest
from udp import LightColoredWifi

class UDP_BaseTestCase(unittest.TestCase):

	def setUp(self):
		self.cls = LightColoredWifi
		self.o = self.cls()

class init_without_arguments(UDP_BaseTestCase):

	def test_sets_ipaddr_to_none(self):
		self.assertEqual(None, self.o.ipaddr)

	def test_sets_port_to_8899(self):
		self.assertEqual(8899, self.o.port)

class init_with_invalid_kwargs(UDP_BaseTestCase):

	def test_raises_exception(self):
		from udp import InvalidKeywordArgument
		self.assertRaises(InvalidKeywordArgument, self.cls, **{'a-invalid-key': None})

class init_with_kwargs_sets_ipaddr(UDP_BaseTestCase):

	def test_ipaddr_set_from_kwargs(self):
		expected_ipaddr = "192.168.1.255"
		self.o = self.cls(**{'ipaddr': expected_ipaddr})
		self.assertEqual(expected_ipaddr, self.o.ipaddr)

class init_with_kwargs_sets_port(UDP_BaseTestCase):

	def test_port_set_from_kwargs(self):
		expected_port = 9999
		self.o = self.cls(**{'port': expected_port})
		self.assertEqual(expected_port, self.o.port)

"""
class send(BaseTestCase):
class turn_on(BaseTestCase):
class set_color(BaseTestCase):
class set_brightness(BaseTestCase):
"""
