import unittest

from console import Console

class init_accepts_empty_any_kwargs(unittest.TestCase):

	def test_init_accepts_any_kwargs(self):
		expected_kwargs = {'foo': "bar"}
		self.o = Console(**expected_kwargs)
		self.assertTrue(None != self.o)

fake_sys_stderr_write_called = None
fake_sys_stderr_write_arg = None
fake_sys_stderr_flush_called = None

class FakeSysStderr():

	def write(self, arg):
		global fake_sys_stderr_write_called
		global fake_sys_stderr_write_arg
		fake_sys_stderr_write_called = True
		fake_sys_stderr_write_arg = arg

	def flush(self):
		global fake_sys_stderr_flush_called
		fake_sys_stderr_flush_called = True

class stderr_write(unittest.TestCase):

	def setUp(self):
		global fake_sys_stderr_write_called
		global fake_sys_stderr_write_arg
		global fake_sys_stderr_flush_called
		fake_sys_stderr_write_called = False
		fake_sys_stderr_write_arg = None
		fake_sys_stderr_flush_called = False
		self.o = Console()

	def test_stderr_write_calls_sys_stderr_write(self):
		import sys
		real_sys_stderr = sys.stderr
		sys.stderr = FakeSysStderr()
		self.o.stderr_write("do not care")
		sys.stderr = real_sys_stderr
		self.assertEqual(True, fake_sys_stderr_write_called)

	def test_stderr_write_calls_sys_stderr_write_with_arg_msg(self):
		fixture = "some failure report message"
		expected = "%s\n" % fixture
		import sys
		real_sys_stderr = sys.stderr
		sys.stderr = FakeSysStderr()
		self.o.stderr_write(fixture)
		sys.stderr = real_sys_stderr
		self.assertEqual(expected, fake_sys_stderr_write_arg)

	def test_stderr_write_calls_sys_stderr_flush(self):
		import sys
		real_sys_stderr = sys.stderr
		sys.stderr = FakeSysStderr()
		self.o.stderr_write("again, we do not care")
		sys.stderr = real_sys_stderr
		self.assertEqual(True, fake_sys_stderr_flush_called)

fake_stderr_write_called = None
fake_stderr_write_arg = None

def fake_stderr_write(arg):
	global fake_stderr_write_called
	global fake_stderr_write_arg
	fake_stderr_write_called = True
	fake_stderr_write_arg = arg

class report_failure(unittest.TestCase):

	def setUp(self):
		global fake_stderr_write_called
		global fake_stderr_write_arg
		fake_stderr_write_called = False
		fake_stderr_write_arg = None
		self.o = Console()
		# Install fake stderr_write()
		self.real_stderr_write = self.o.stderr_write
		self.o.stderr_write = fake_stderr_write

	def tearDown(self):
		self.o.stderr_write = self.real_stderr_write

	def test_report_failure_calls_stderr_write(self):
		self.o.report_failure("https://some-host")
		self.assertEqual(True, fake_stderr_write_called)

	def test_report_failure_calls_stderr_write_with_expected_arg(self):
		expected = "Failure reported by http://jenkins!"
		self.o.report_failure("http://jenkins")
		self.assertEqual(expected, fake_stderr_write_arg)

fake_report_failure_called = None
fake_report_failure_args = None

def fake_report_failure(arg):
	global fake_report_failure_called
	global fake_report_failure_args
	fake_report_failure_called += 1
	fake_report_failure_args.append(arg)

class update(unittest.TestCase):

	def setUp(self):
		global fake_report_failure_called
		global fake_report_failure_args
		fake_report_failure_called = 0
		fake_report_failure_args = []
		self.o = Console()
		self.o.report_failure = fake_report_failure

	def test_update_does_not_call_report_failure_when_report_empty(self):
		report = []
		self.o.update(report)
		self.assertEqual(len(report), fake_report_failure_called)

	def test_update_calles_report_failure_for_each_report_entry(self):
		report = [{'msg': 1}, {'msg': 2}, {'msg': 3}, {'msg': 4}, {'msg': 5}, {'msg': 6}]
		self.o.update(report)
		self.assertEqual(len(report), fake_report_failure_called)

	def test_update_calls_report_failure_with_msg_from_report_entry(self):
		report = [
			{'msg': "Hello"},
			{'msg': "World"}
		]
		expected = ["Hello", "World"]
		self.o.update(report)
		self.assertEqual(expected, fake_report_failure_args)
