class InvalidKeywordArgument(Exception):
	pass

class LightColoredWifi():

	def __init__(self, **kwargs):
		ipaddr = None
		port = 8899
		for arg_key in kwargs.keys():
			arg_val = kwargs[arg_key]
			if arg_key is "ipaddr":
				ipaddr = arg_val
			elif arg_key is "port":
				port = arg_val
			else:
				raise InvalidKeywordArgument
		self.ipaddr = ipaddr
		self.port = port
