import unittest
from ambient_orb import AmbientOrb

class AmbientOrb_BaseTestCase(unittest.TestCase):

	def setUp(self):
		self.a = AmbientOrb()

	def clear_outputs_configured(self):
		self.a._outputs_configured = []

class get_inputs_supported(AmbientOrb_BaseTestCase):

	def test_get_inputs_supported_returns_list_with_single_entry_for_jenkins(self):
		self.assertEqual(["jenkins"], self.a.get_inputs_supported())

class get_outputs_supported(AmbientOrb_BaseTestCase):

	def test_get_outputs_supported_returns_list(self):
		expected = ["console", "ifttt"]
		self.assertEqual(expected, self.a.get_outputs_supported())

class get_inputs_configured(AmbientOrb_BaseTestCase):

	def test_get_inputs_configured_returns_empty_list(self):
		self.assertEqual([], self.a.get_inputs_configured())

class get_outputs_configured(AmbientOrb_BaseTestCase):

	def test_get_outputs_configured_returns_list_with_single_entry_for_console(self):
		self.assertEqual([{'kind': "console", 'args': {}}], self.a.get_outputs_configured())

class add_input(AmbientOrb_BaseTestCase):

	def test_add_input_appends_inputs_configured(self):
		self.a.add_input("jenkins", "https://localhost/jenkins1")
		self.a.add_input("jenkins", "https://localhost/jenkins2")
		self.a.add_input("jenkins", "https://localhost/jenkins3")
		self.assertEqual(3, len(self.a.get_inputs_configured()))

	def test_add_input_appends_dict_to_inputs_configured(self):
		expected_url = "http://localhost/jenkins10"
		expected_kind = "jenkins"
		expected = {'url': expected_url, 'kind': expected_kind}
		self.a.add_input(expected_kind, expected_url)
		actual = self.a._inputs_configured[0]
		self.assertEqual(expected, actual)

class add_input_unsupported(AmbientOrb_BaseTestCase):

	def test_add_input_raises_exception_when_kind_is_invalid(self):
		args = ["unsupported-ci-system", "http://localhost/some-ci-system"]
		from ambient_orb import UnsupportedInput
		self.assertRaises(UnsupportedInput, self.a.add_input, *args)

class add_output(AmbientOrb_BaseTestCase):

	def test_add_output_appends_outputs_configured(self):
		self.clear_outputs_configured()
		self.a.add_output("console")
		self.a.add_output("console")
		self.a.add_output("console")
		self.assertEqual(3, len(self.a.get_outputs_configured()))

	def test_add_output_appends_dict_to_outputs_configured(self):
		self.clear_outputs_configured()
		expected_kind = "console"
		expected = {'kind': expected_kind, 'args': {}}
		self.a.add_output(expected_kind)
		actual = self.a._outputs_configured[0]
		self.assertEqual(expected, actual)

	def test_add_output_inserts_args_into_dict(self):
		args = {'key': "xxx", 'event_name': "some_event_name"}
		expected = {'kind': "console", 'args': args}
		self.a.add_output("console", args)
		actual = self.a._outputs_configured[-1]
		self.assertEqual(expected, actual)

class add_output_unsupported(AmbientOrb_BaseTestCase):

	def test_add_output_raises_exception_when_kind_is_invalid(self):
		args = ["crazy-output"]
		from ambient_orb import UnsupportedOutput
		self.assertRaises(UnsupportedOutput, self.a.add_output, *args)

fake_input_init_called = 0
fake_input_set_base_url_called = 0
fake_input_set_base_url_args = {}
fake_input_has_failed_called = 0
fake_input_has_failed_retval = None

class FakeInput():

	def __init__(self):
		global fake_input_init_called
		fake_input_init_called += 1

	def set_base_url(self, base_url):
		global fake_input_set_base_url_called
		global fake_input_set_base_url_args
		fake_input_set_base_url_args.update({fake_input_set_base_url_called: base_url})
		fake_input_set_base_url_called += 1

	def has_failed(self):
		global fake_input_has_failed_called
		global fake_input_has_failed_retval
		fake_input_has_failed_called += 1
		return fake_input_has_failed_retval

class dispatch_inputs(unittest.TestCase):

	def setUp(self):
		self.expected_n_inputs_configured = 3
		self.expected_base_url = "http://10.0.0.1:8080"

		global fake_input_init_called
		global fake_input_set_base_url_called
		global fake_input_set_base_url_args
		global fake_input_has_failed_called
		global fake_input_has_failed_retval

		fake_input_init_called = 0
		fake_input_set_base_url_called = 0
		fake_input_set_base_url_args = {}
		fake_input_has_failed_called = 0
		fake_input_has_failed_retval = []

		self.a = AmbientOrb()
		self.a.inputs_cls['jenkins'] = FakeInput
		for i in range(self.expected_n_inputs_configured):
			self.a.add_input("jenkins", self.expected_base_url)

	def test_dispatch_inputs_creates_input_obj_for_each_input_configured(self):
		expected = self.expected_n_inputs_configured
		self.a._dispatch_inputs()
		self.assertEqual(expected, fake_input_init_called)

	def test_dispatch_inputs_calls_set_base_url_on_each_obj_from_inputs_configured(self):
		expected = self.expected_n_inputs_configured
		self.a._dispatch_inputs()
		self.assertEqual(expected, fake_input_set_base_url_called)

	def test_dispatch_inputs_calls_set_base_url_with_arg_url_from_entry_in_inputs_configured(self):
		expected = {}
		for i in range(self.expected_n_inputs_configured):
			expected.update({i: self.expected_base_url})
		self.a._dispatch_inputs()
		self.assertEqual(expected, fake_input_set_base_url_args)

	def test_dispatch_inputs_calls_has_failed_on_each_input_obj_from_inputs_configured(self):
		expected = self.expected_n_inputs_configured
		self.a._dispatch_inputs()
		self.assertEqual(expected, fake_input_has_failed_called)

	def test_dispatch_inputs_returns_report_list(self):
		self.maxDiff = None
		# This is what we expected has_failed() to return.
		# Contains only 'job-name' and 'job-state' keys.
		has_failed_fixture = []
		has_failed_fixture.append({'job-name': "some-build-job", 'job-state': "broken since this build"})
		global fake_input_has_failed_retval
		fake_input_has_failed_retval = has_failed_fixture
		# The expected dict will contain two more keys.
		# 'kind' and 'url'.
		# It is the responsibility of 'dispatch_inputs()'
		# to add these keys and assign values.
		expected = []
		for i in range(self.expected_n_inputs_configured):
			entry = {}
			entry.update({'kind': "jenkins"})
			entry.update({'url': self.expected_base_url})
			entry.update({'job-name': "some-build-job"})
			entry.update({'job-state': "broken since this build"})
			expected.append(entry)
		report = self.a._dispatch_inputs()
		self.assertEqual(expected, report)

fake_output_created = 0
fake_output_kwargs = None
fake_output_update_called = 0
fake_output_update_arg = None

class FakeOutput():

	def __init__(self, **kwargs):
		global fake_output_created
		global fake_output_kwargs
		fake_output_created += 1
		fake_output_kwargs = kwargs

	def update(self, arg):
		global fake_output_update_called
		global fake_output_update_arg
		fake_output_update_called += 1
		fake_output_update_arg = arg

class dispatch_outputs(unittest.TestCase):

	def setUp(self):
		global fake_output_created
		global fake_output_kwargs
		global fake_output_update_called
		global fake_output_update_arg

		fake_output_created = 0
		fake_output_kwargs = None
		fake_output_update_called = 0
		fake_output_update_arg = None

		self.a = AmbientOrb()
		self.a.outputs_cls['console'] = FakeOutput

	def test_dispatch_outputs_creates_output_obj_for_each_output_configured(self):
		report = []
		self.a._dispatch_outputs(report)
		self.assertEqual(1, fake_output_created)

	def test_dispatch_outputs_creates_output_obj_with_args_from_dict(self):
		expected = {'key': "foo", 'event_name': "bar"}
		args = expected
		report = []
		self.a.add_output("console", args)
		self.a._dispatch_outputs(report)
		self.assertEqual(expected, fake_output_kwargs)

	def test_dispatch_outputs_calls_update_on_each_input_obj_from_outputs_configured(self):
		report = []
		self.a._dispatch_outputs(report)
		self.assertEqual(1, fake_output_update_called)

	def test_dispatch_outputs_calls_update_with_arg_report(self):
		self.maxDiff = None
		report = [
			{'kind': "jenkins", 'url': "http://10.0.0.1:8080", 'job-name': "some job 1", 'job-state': "broken since this build"},
			{'kind': "jenkins", 'url': "http://10.0.0.1:8080", 'job-name': "some job 2", 'job-state': "broken since this build"},
			{'kind': "jenkins", 'url': "http://10.0.0.1:8080", 'job-name': "some job 3", 'job-state': "broken since this build"}
		]
		expected = report
		self.a._dispatch_outputs(report)
		self.assertEqual(expected, fake_output_update_arg)

fake_dispatch_inputs_called = None
fake_dispatch_inputs_retval = None

def fake_dispatch_inputs():
	global fake_dispatch_inputs_called
	global fake_dispatch_inputs_retval
	fake_dispatch_inputs_called = True
	return fake_dispatch_inputs_retval

fake_dispatch_outputs_called = None
fake_dispatch_outputs_arg = None

def fake_dispatch_outputs(arg):
	global fake_dispatch_outputs_called
	global fake_dispatch_outputs_arg
	fake_dispatch_outputs_called = True
	fake_dispatch_outputs_arg = arg

class run(unittest.TestCase):

	def setUp(self):
		self.expected_n_servers = 5
		self.expected_base_url = "http://localhost:8080"
		self.expected_arg_to_dispatch_outputs = ["a-magic-value"]

		global fake_dispatch_inputs_called
		global fake_dispatch_inputs_retval
		global fake_dispatch_outputs_called
		global fake_dispatch_outputs_arg

		fake_dispatch_inputs_called = False
		fake_dispatch_inputs_retval = self.expected_arg_to_dispatch_outputs
		fake_dispatch_outputs_called = False
		fake_dispatch_outputs_arg = None

		self.a = AmbientOrb()
		self.a._dispatch_inputs = fake_dispatch_inputs
		self.a._dispatch_outputs = fake_dispatch_outputs
		for i in range(self.expected_n_servers):
			self.a.add_input("jenkins", self.expected_base_url)

	def test_run_raises_exception_when_no_input_configured(self):
		from ambient_orb import NoInputConfigured
		self.a._inputs_configured = []
		self.assertRaises(NoInputConfigured, self.a.run)

	def test_run_raises_exception_when_no_output_configured(self):
		from ambient_orb import NoOutputConfigured
		self.a._outputs_configured = []
		self.assertRaises(NoOutputConfigured, self.a.run)

	def test_run_calls_dispatch_inputs(self):
		self.a.run()
		self.assertEqual(True, fake_dispatch_inputs_called)

	def test_run_calls_dispatch_outputs(self):
		self.a.run()
		self.assertEqual(True, fake_dispatch_outputs_called)

	def test_run_calls_dispatch_outputs_with_retval_from_dispatch_inputs_as_arg(self):
		self.a.run()
		self.assertEqual(self.expected_arg_to_dispatch_outputs, fake_dispatch_outputs_arg)
