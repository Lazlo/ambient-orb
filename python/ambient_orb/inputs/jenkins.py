class InvalidKeywordArgument(Exception):
	pass

class NoUrlSet(Exception):
	pass

class JenkinsRSS(object):

	def __init__(self, **kwargs):
		# Parse keyword arguments
		base_url = None
		for arg_key in kwargs.keys():
			arg_val = kwargs[arg_key]
			if arg_key is 'base_url':
				base_url = arg_val
			else:
				raise InvalidKeywordArgument
		# Initialize internals
		self.xml_ns = "http://www.w3.org/2005/Atom"
		self.expected_tag_entry = "{%s}%s" % (self.xml_ns, "entry")
		self.expected_tag_title = '{%s}%s' % (self.xml_ns, 'title')
		self.job_passed_markers = ["stable", "unstable", "aborted", "not built", "back to normal", "?"]
		# Save constructor arguments
		self._base_url = base_url

	def set_base_url(self, base_url):
		self._base_url = base_url

	def url_for_rss(self):
		return "%s/rssAll" % self._base_url

	def url_for_rss_failed(self):
		return "%s/rssFailed" % self._base_url

	def url_for_rss_latest(self):
		return "%s/rssLatest" % self._base_url

	def url_for_job(self, job_name):
		return "%s/job/%s" % (self._base_url, job_name)

	def url_for_job_rss(self, job_name):
		return "%s/rssAll" % self.url_for_job(job_name)

	def url_for_job_rss_failed(self, job_name):
		return "%s/rssFailed" % self.url_for_job(job_name)

	def fetch(self):
		if not self._base_url:
			raise NoUrlSet
		url = self.url_for_rss_latest()
		import urllib2
		s = urllib2.urlopen(url)
		rss = s.read()
		return rss

	def parse_tag_title_text(self, tag_text):
		res = {}
		import re
		m = re.match('(?P<job_name>.*) #[0-9]+ \((?P<job_state>.*)\)', tag_text)
		if not m:
			return res
		res.update({'job-name': m.group("job_name")})
		res.update({'job-state': m.group("job_state")})
		return res

	def job_state_is_failed(self, job_state):
		if job_state in self.job_passed_markers:
			return False
		# For ...
		# "broken for a long time"
		# "broken since this build"
		# "broken since build #N"
		return True

	def _job_in_list(self, job_name, job_list):
		for job in job_list:
			if job['job-name'] == job_name:
				return True
		return False

	def parse_rss(self, rss):
		list_failed = []
		import xml.etree.ElementTree as ET
		root = ET.fromstring(rss)
		# TODO Check if we can do the same with root.findall()
		for child in root:
			# Skip tags that are not <entry>
			if child.tag != self.expected_tag_entry:
				continue
			# Find the <title> child of <entry>
			tag_title = child.find(self.expected_tag_title)
			if tag_title == None:
				# WARNING: Failed to find <titlte> tag
				continue
			job = self.parse_tag_title_text(tag_title.text)
			if len(job) == 0:
				# WARNING: Failed to extract info from <title> text
				continue
			job_state = job['job-state']
			# Determine job failure state
			job_failed = self.job_state_is_failed(job_state)
			if not job_failed:
				continue
			job_name = job['job-name']
			# Check if there is already and entry for this job.
			if self._job_in_list(job_name, list_failed):
				continue
			# NOTE We want to add a field called 'msg' that will
			# be common to all reports returned by input classes.
			# This field can the be picked up by output classes,
			# without having to understand what the message is saying.
			# Assuming, entries in report always qualify for notification
			# of some output class.
			msg = "%s at %s reported %s %s" % ("Jenkins", self._base_url, job_name, job_state)
			job.update({'msg': msg})
			list_failed.append(job)
		return list_failed

	def has_failed(self):
		rss = self.fetch()
		return self.parse_rss(rss)
