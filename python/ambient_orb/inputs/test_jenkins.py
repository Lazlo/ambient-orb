import unittest

from jenkins import JenkinsRSS

class JenkinsRSS_BaseTestCase(unittest.TestCase):

	def setUp(self):
		self.expected_base_url = "https://localhost:8008"
		self.i = JenkinsRSS(base_url=self.expected_base_url)

class object_created_with_kwarg_base_url(JenkinsRSS_BaseTestCase):

	def test_init_sets_base_url_from_kwarg(self):
		self.assertEqual(self.expected_base_url, self.i._base_url)

class object_created_with_invalid_kwarg_raises_exception(JenkinsRSS_BaseTestCase):

	def test_init_raises_exception_on_invalid_kwarg(self):
		from jenkins import InvalidKeywordArgument
		self.assertRaises(InvalidKeywordArgument, JenkinsRSS, **{'invalid_key': "random-value"})

class set_base_url(JenkinsRSS_BaseTestCase):

	def test_set_base_url(self):
		# Make sure to clear JekinsRSS._base_url that has been
		# set in our BaseTestCase.setUp() by passsing it to the
		# consturctor of JenkinsRSS.
		self.i._base_url = None
		self.i.set_base_url(self.expected_base_url)
		self.assertEqual(self.expected_base_url, self.i._base_url)

class url_for_rss(JenkinsRSS_BaseTestCase):

	def test_url_for_rss(self):
		expected = "%s/rssAll" % self.expected_base_url
		self.assertEqual(expected, self.i.url_for_rss())

class url_for_rss_failed(JenkinsRSS_BaseTestCase):

	def test_url_for_rss_failed(self):
		expected = "%s/rssFailed" % (self.expected_base_url)
		self.assertEqual(expected, self.i.url_for_rss_failed())

class url_for_rss_latest(JenkinsRSS_BaseTestCase):

	def test_url_for_rss_latest(self):
		expected = "%s/rssLatest" % self.expected_base_url
		self.assertEqual(expected, self.i.url_for_rss_latest())

class url_for_job(JenkinsRSS_BaseTestCase):

	def test_url_for_job(self):
		job_name = "my-test-job"
		expected = "%s/job/%s" % (self.expected_base_url, job_name)
		self.assertEqual(expected, self.i.url_for_job(job_name))

class url_for_job_rss(JenkinsRSS_BaseTestCase):

	def test_url_for_job_rss(self):
		job_name = "my-2nd-testjob"
		expected = "%s/job/%s/rssAll" % (self.expected_base_url, job_name)
		self.assertEqual(expected, self.i.url_for_job_rss(job_name))

class url_for_job_rss_failed(JenkinsRSS_BaseTestCase):

	def test_url_for_job_rss_failed(self):
		job_name = "some-other-job"
		expected = "%s/job/%s/rssFailed" % (self.expected_base_url, job_name)
		self.assertEqual(expected, self.i.url_for_job_rss_failed(job_name))

# Mock used as return value for fake urllib2.urlopen().read()
class FakeSocket():

	def __init__(self):
		self.read_called = False
		self.read_retval = None

	def read(self):
		self.read_called = True
		return self.read_retval

# Fake used for urllib2.urlopen()
fake_urlopen_called = None
fake_urlopen_arg = None
fake_urlopen_retval = None

def fake_urlopen(arg):
	global fake_urlopen_called
	global fake_urlopen_arg
	fake_urlopen_called = True
	fake_urlopen_arg = arg
	return fake_urlopen_retval

import urllib2

class fetch(unittest.TestCase):

	def setUp(self):
		self.setup_fixtures()
		# Set up fake HTTP GET repsonse contents
		self.expected_http_get_resp = open(self.fixture_rss_failed_with_failures).read()
		self.fake_socket_obj = FakeSocket()
		self.fake_socket_obj.read_retval = self.expected_http_get_resp
		self.real_urlopen = urllib2.urlopen
		self.fake_urlopen = fake_urlopen
		urllib2.urlopen = self.fake_urlopen
		global fake_urlopen_called
		global fake_urlopen_arg
		global fake_urlopen_retval
		fake_urlopen_called = False
		fake_urlopen_arg = None
		fake_urlopen_retval = self.fake_socket_obj
		self.expected_base_url = "http://localhost:8080"
		self.i = JenkinsRSS()
		self.i.set_base_url(self.expected_base_url)

	def setup_fixtures(self):
		import os
		self.fixtures_dir = "test-fixtures"
		self.fixtures_path = os.path.join(os.path.dirname(__file__), self.fixtures_dir)
		self.fixture_rss_failed_no_failures = "%s/jenkins-rssFailed-no-failures.xml" % self.fixtures_path
		self.fixture_rss_failed_with_failures = "%s/jenkins-rssFailed-with-failures.xml" % self.fixtures_path

	def tearDown(self):
		urllib2.urlopen = self.real_urlopen

	def test_fetch_with_base_url_raises_no_url_set_exception(self):
		from jenkins import NoUrlSet
		self.i._base_url = None
		self.assertRaises(NoUrlSet, self.i.fetch)

	def test_fetch_calls_urllib2_urlopen(self):
		self.i.fetch()
		self.assertEqual(True, fake_urlopen_called)

	def test_fetch_calls_urllib2_urlopen_with_arg_url_to_rss_latest(self):
		expected_url = "%s/rssLatest" % self.expected_base_url
		self.i.set_base_url(self.expected_base_url)
		self.i.fetch()
		self.assertEqual(expected_url, fake_urlopen_arg)

	def test_fetch_calls_socket_read(self):
		self.i.fetch()
		self.assertEqual(True, self.fake_socket_obj.read_called)

	def test_fetch_returns_retval_from_socket_read(self):
		self.assertEqual(self.fake_socket_obj.read_retval, self.i.fetch())

class parse_tag_title_text(JenkinsRSS_BaseTestCase):

	def test_parse_tag_title_text_returns_empty_dict_on_no_match(self):
		self.assertEqual({}, self.i.parse_tag_title_text("..."))

	def test_parse_tag_title_text_returns_dict_with_job_name(self):
		job_name = "crazy-robot"
		fixture = "%s #47 (broken since build #3)" % job_name
		expected = job_name
		res = self.i.parse_tag_title_text(fixture)
		actual = res['job-name']
		self.assertEqual(expected, actual)

	def test_parse_tag_title_text_returns_dict_with_job_state(self):
		job_name = "crazy-robot"
		job_state = "broken since build #3"
		fixture = "%s #47 (%s)" % (job_name, job_state)
		expected = job_state
		res = self.i.parse_tag_title_text(fixture)
		actual = res['job-state']
		self.assertEqual(expected, actual)

class job_state_is_failed(JenkinsRSS_BaseTestCase):

	def test_job_state_is_failed_returns_false_when_state_was_stable(self):
		fixture = "stable"
		self.assertEqual(False, self.i.job_state_is_failed(fixture))

	def test_job_state_is_failed_returns_false_when_state_was_unstable(self):
		fixture = "unstable"
		self.assertEqual(False, self.i.job_state_is_failed(fixture))

	def test_job_state_is_failed_returns_false_when_state_was_aborted(self):
		fixture = "aborted"
		self.assertEqual(False, self.i.job_state_is_failed(fixture))

	def test_job_state_is_failed_returns_false_when_state_was_not_built(self):
		fixture = "not built"
		self.assertEqual(False, self.i.job_state_is_failed(fixture))

	def test_job_state_is_failed_returns_false_when_state_was_back_to_normal(self):
		fixture = "back to normal"
		self.assertEqual(False, self.i.job_state_is_failed(fixture))

	def test_job_state_is_failed_returns_false_when_state_was_questionmark(self):
		fixture = "?"
		self.assertEqual(False, self.i.job_state_is_failed(fixture))

	def test_job_state_is_failed_returns_true_when_state_was_broken_for_a_long_time(self):
		fixture = "broken for a long time"
		self.assertEqual(True, self.i.job_state_is_failed(fixture))

	def test_job_state_is_failed_returns_true_when_state_was_broken_since_this_build(self):
		fixture = "broken since this build"
		self.assertEqual(True, self.i.job_state_is_failed(fixture))

	def test_job_state_is_failed_returns_true_when_state_was_broken_since_build_N(self):
		fixture = "broken since build #3"
		self.assertEqual(True, self.i.job_state_is_failed(fixture))


class has_failed(unittest.TestCase):

	def setUp(self):
		self.setup_fixtures()
		# Fake Socket returned by urllib2.urlopen()
		self.fake_socket_obj = FakeSocket()
		# Fake urllib2.urlopen()
		self.real_urlopen = urllib2.urlopen
		self.fake_urlopen = fake_urlopen
		urllib2.urlopen = self.fake_urlopen
		global fake_urlopen_retval
		fake_urlopen_retval = self.fake_socket_obj
		# UUT setup
		self.expected_base_url = "http://localhost:8080"
		self.i = JenkinsRSS()
		self.i.set_base_url(self.expected_base_url)

	def setup_fixtures(self):
		import os
		self.fixtures_dir = "test-fixtures"
		self.fixtures_path = os.path.join(os.path.dirname(__file__), self.fixtures_dir)
		self.fixture_rss_failed_no_failures = "%s/jenkins-rssFailed-no-failures.xml" % self.fixtures_path
		self.fixture_rss_failed_with_failures = "%s/jenkins-rssFailed-with-failures.xml" % self.fixtures_path
		self.fixture_rss_latest_no_failures = "%s/jenkins-rssLatest-no-failures.xml" % self.fixtures_path
		self.fixture_rss_latest_with_failures = "%s/jenkins-rssLatest-with-failures.xml" % self.fixtures_path

	def tearDown(self):
		urllib2.urlopen = self.real_urlopen

	def test_has_failed_returns_list_of_broken_jobs_from_rss_latest(self):
		expected = []
		expected.append({'job-name': "solo-cc2640r2-firmware"	, 'job-state': "broken since build #3", 'msg': "Jenkins at http://localhost:8080 reported solo-cc2640r2-firmware broken since build #3"})
		expected.append({'job-name': "solo-testboard"		, 'job-state': "broken for a long time", 'msg': "Jenkins at http://localhost:8080 reported solo-testboard broken for a long time"})
		self.fake_socket_obj.read_retval = open(self.fixture_rss_latest_with_failures).read()
		self.assertEqual(expected, self.i.has_failed())

	def test_has_failed_returns_empty_list_of_broken_jobs_from_rss_latest(self):
		self.fake_socket_obj.read_retval = open(self.fixture_rss_latest_no_failures).read()
		self.assertEqual([], self.i.has_failed())

	def test_has_failed_returns_list_without_duplicate_entries_from_rss_failed(self):
		self.maxDiff = None
		expected = []
		expected.append({'job-name': "solo-ci"			, 'job-state': "broken for a long time", 'msg': "Jenkins at http://localhost:8080 reported solo-ci broken for a long time"})
		expected.append({'job-name': "solo-tester"		, 'job-state': "broken since build #33", 'msg': "Jenkins at http://localhost:8080 reported solo-tester broken since build #33"})
		expected.append({'job-name': "solo-optee_su_ta"		, 'job-state': "broken since build #533", 'msg': "Jenkins at http://localhost:8080 reported solo-optee_su_ta broken since build #533"})
		self.fake_socket_obj.read_retval = open(self.fixture_rss_failed_with_failures).read()
		self.assertEqual(expected, self.i.has_failed())
