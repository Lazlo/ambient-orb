# Ambient Orb

Allows performing checks for build failures using a list of CI servers and provide feedback accordingly (e.g. by means of a controllable color light bulb).

Ambient Orb is being developed test-driven (test first!) and is implemented in Python 2.7.

## Features

 * supported CI servers (inputs)
   - Jenkins
 * supported feedback methods (outputs)
   - stderr
   - IFTTT color light bulb
